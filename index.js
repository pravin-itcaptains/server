const path = require("path");
const express = require("express");
const app = express(); // create express app

app.use(express.static(path.join(__dirname, "build")));

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "index.html"));
});


const port = process.env.PORT || 5000
    // start express server on port 5000
app.listen(port, () => {
    console.log("server started on port", port);
});